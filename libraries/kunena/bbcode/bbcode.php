<?php
/**
 * Kunena Component
 * @package Kunena.Framework
 * @subpackage BBCode
 *
 * @copyright (C) 2008 - 2016 Kunena Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link https://www.kunena.org
 **/
defined ( '_JEXEC' ) or die ();

require_once KPATH_FRAMEWORK . '/external/nbbc/nbbc.php';
jimport('joomla.utilities.string');

// TODO: add possibility to hide contents from these tags:
// [hide], [confidential], [spoiler], [attachment], [code]

/**
 * Class KunenaBbcode
 *
 * @since   2.0
 */
class KunenaBbcode extends NBBC_BBCode
{
	public $autolink_disable = 0;

	/**
	 * @var object
	 */
	public $parent = null;

	/**
	 * Use KunenaBbcode::getInstance() instead.
	 *
	 * @param bool $relative
	 * @internal
	 */
	public function __construct($relative = true)
	{
		parent::__construct();
		$this->defaults = new KunenaBbcodeLibrary;
		$this->tag_rules = $this->defaults->default_tag_rules;

		$this->smileys = $this->defaults->default_smileys;

		if (empty($this->smileys))
		{
			$this->SetEnableSmileys(false);
		}

		$this->SetSmileyDir ( JPATH_ROOT );
		$this->SetSmileyURL ( $relative ? JUri::root(true) : rtrim(JUri::root(), '/') );
		$this->SetDetectURLs ( true );
		$this->SetURLPattern (array($this, 'parseUrl'));
		$this->SetURLTarget('_blank');

		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('kunena');
		$dispatcher->trigger( 'onKunenaBbcodeConstruct', array( $this ) );
	}

	/**
	 * Get global instance from BBCode parser.
	 *
	 * @param bool $relative
	 * @return mixed
	 */
	public static function getInstance($relative = true)
	{
		static $instance = false;

		if (!isset($instance[intval($relative)]))
		{
			$instance[intval($relative)] = new KunenaBbcode ($relative);
		}

		$instance[intval($relative)]->autolink_disable = 0;

		return $instance[intval($relative)];
	}

	/**
	 * @param $params
	 * @return string
	 */
	public function parseUrl($params)
	{
		$url = $params['url'];
		$text = $params['text'];

		$config = KunenaFactory::getConfig ();
		if ($config->autolink)
		{
			if (preg_match('#^mailto:#ui', $url))
			{
				// Cloak email addresses
				$email = substr($text, 7);

				$layout = KunenaLayout::factory('BBCode/Email');

				if ($layout->getPath())
				{
					return (string) $layout
						->set('email', $email)
						->set('mailto', $this->IsValidEmail($email));
				}

				if ($this->canCloakEmail($params))
				{
					return JHtml::_('email.cloak', $email, $this->IsValidEmail($email));
				}
				else
				{
					return '<a href="mailto:' . $email . '">' . $email . '</a>';
				}
			}

			// Remove http(s):// from the text
			$text = preg_replace ( '#^http(s?)://#ui', '', $text );

			if ($config->trimlongurls)
			{
				// shorten URL text if they are too long
				$text = preg_replace ( '#^(.{' . $config->trimlongurlsfront . '})(.{4,})(.{' . $config->trimlongurlsback . '})$#u', '\1...\3', $text );
			}
		}

		if (!isset($params['query']))
		{
			$params['query'] = '';
		}

		if (!isset($params['path']))
		{
			$params['path'] = '';
		}

		if ($config->autoembedyoutube && empty($this->parent->forceMinimal) && isset($params['host']))
		{
			// convert youtube links to embedded player
			parse_str($params['query'], $query);
			$path = explode('/', $params['path']);

			if (strstr($params['host'], '.youtube.') && !empty($path[1]) && $path[1]=='watch' && !empty($query['v']))
			{
				$video = $query['v'];
			}
			elseif ($params['host'] == 'youtu.be' && !empty($path[1]))
			{
				$video = $path[1];
			}

			if (isset($video))
			{
				$uri = JURI::getInstance();

				if ($uri->isSSL())
				{
					return '<iframe width="425" height="344" src="https://www.youtube.com/embed/' . urlencode($video) . '" frameborder="0" allowfullscreen></iframe>';
				}
				else
				{
					return '<iframe width="425" height="344" src="http://www.youtube.com/embed/' . urlencode($video) . '" frameborder="0" allowfullscreen></iframe>';
				}
			}
		}

		if ($config->autoembedebay && empty($this->parent->forceMinimal) && isset($params['host']) && strstr($params['host'], '.ebay.'))
		{
			parse_str($params['query'], $query);
			$path = explode('/', $params['path']);

			if ($path[1] == 'itm')
			{
				if (isset($path[3]) && is_numeric($path[3]))
				{
					$itemid = $path[3];
				}
				elseif (isset($path[2]) && is_numeric($path[2]))
				{
					$itemid = $path[2];
				}

				if (isset($itemid))
				{
					// convert ebay item to embedded widget
					KunenaBbcodeLibrary::renderEbayLayout($itemid);
				}

				return $this->defaults->getEbayItemFromCache($itemid);
			}

			parse_str($params['query'], $query);

			// FIXME: ebay search and seller listings are not supported.
			if (isset($path[1]) && $path[1] == 'sch' && !empty($query['_nkw']))
			{
				// convert ebay search to embedded widget
				KunenaBbcodeLibrary::renderEbayLayout($itemid);

				// TODO: Remove in Kunena 4.0
				return '<object width="355" height="300"><param name="movie" value="http://togo.ebay.com/togo/togo.swf?2008013100" /><param name="flashvars" value="base=http://togo.ebay.com/togo/&lang=' . $config->ebay_language . '&mode=search&query='
				. urlencode($query['_nkw']) .'&campid='.$config->ebay_affiliate_id.'" /><embed src="http://togo.ebay.com/togo/togo.swf?2008013100" type="application/x-shockwave-flash" width="355" height="300" flashvars="base=http://togo.ebay.com/togo/&lang='
				. $config->ebay_language . '&mode=search&query=' . urlencode($query['_nkw']) . '&campid='.$config->ebay_affiliate_id.'"></embed></object>';

			}

			if (strstr($params['host'], 'myworld.') && !empty($path[1]))
			{
				// convert seller listing to embedded widget
				$layout = KunenaLayout::factory('BBCode/eBay');

				if ($layout->getPath())
				{
					return (string) $layout
						->set('content', urlencode($path[1]))
						->set('params', null)
						->set('width', 355)
						->set('height', 355)
						->set('language', $config->ebaylanguagecode)
						->set('affiliate', $config->ebay_affiliate_id)
						->setLayout('seller');
				}

				// TODO: Remove in Kunena 4.0
				return '<object width="355" height="355"><param name="movie" value="http://togo.ebay.com/togo/seller.swf?2008013100" /><param name="flashvars" value="base=http://togo.ebay.com/togo/&lang='
				. $config->ebay_language . '&seller=' . urlencode($path[1]) . '&campid='.$config->ebay_affiliate_id.'" /><embed src="http://togo.ebay.com/togo/seller.swf?2008013100" type="application/x-shockwave-flash" width="355" height="355" flashvars="base=http://togo.ebay.com/togo/&lang='
				. $config->ebay_language . '&seller=' . urlencode($path[1]) . '&campid='.$config->ebay_affiliate_id.'"></embed></object>';
			}
		}

		if (isset($params['host']) && strstr($params['host'], 'twitter.') )
		{
			$path = explode('/', $params['path']);

			if ( isset($path[3]) )
			{
				return $this->defaults->renderTweet($path[3]);
			}
		}

		if ($config->autolink)
		{
			$layout = KunenaLayout::factory('BBCode/URL');

			if ($layout->getPath())
			{
				return (string) $layout
					->set('content', $text)
					->set('url', $url)
					->set('target', $this->url_target);
			}

			$url = htmlspecialchars($url, ENT_COMPAT, 'UTF-8');
			if (strpos($url, '/index.php') !== 0)
			{
				return "<a class=\"bbcode_url\" href=\"{$url}\" target=\"_blank\" rel=\"nofollow\">{$text}</a>";
			}
			else
			{
				return "<a class=\"bbcode_url\" href=\"{$url}\" target=\"_blank\">{$text}</a>";
			}
		}

		// Auto-linking has been disabled.
		return $text;
	}

	/**
	 * @param $string
	 * @return array
	 */
	function Internal_AutoDetectURLs($string)
	{
		$search = preg_split('/(?xi)
		\b
		(
			(?:
				(?:https?|ftp):\/\/
				|
				www\d{0,3}\.
				|
				mailto:
				|
				(?:[a-zA-Z0-9._-]{2,}@)
			)
			(?:
				[^\s()<>]+
				|
				\((?:[^\s()<>]+|(\(?:[^\s()<>]+\)))*\)
			)+
			(?:
				\((?:[^\s()<>]+|(\(?:[^\s()<>]+\)))*\)
				|
				[^\s`!()\[\]{};:\'"\.,<>?Р’В«Р’В»РІР‚СљРІР‚СњРІР‚