<?php
/**
 * Joomla! Content Management System
 *
 * @copyright  Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Joomla\CMS\Language;

defined('JPATH_PLATFORM') or die;

/**
 * Class to transliterate strings
 *
 * @since  11.1
 * @note   Port of phputf8's utf8_accents_to_ascii()
 */
class Transliterate
{
	/**
	 * Returns strings transliterated from UTF-8 to Latin
	 *
	 * @param   string   $string  String to transliterate
	 * @param   integer  $case    Optionally specify upper or lower case. Default to null.
	 *
	 * @return  string  Transliterated string
	 *
	 * @since   11.1
	 */
	public static function utf8_latin_to_ascii($string, $case = 0)
	{
		static $UTF8_LOWER_ACCENTS = null;
		static $UTF8_UPPER_ACCENTS = null;

		if ($case <= 0)
		{
			if (is_null($UTF8_LOWER_ACCENTS))
			{
				$UTF8_LOWER_ACCENTS = array(
					'Р“В ' => 'a',
					'Р“Т‘' => 'o',
					'Р”РЏ' => 'd',
					'Р±С‘Сџ' => 'f',
					'Р“В«' => 'e',
					'Р•РЋ' => 's',
					'Р–РЋ' => 'o',
					'Р“Сџ' => 'ss',
					'Р”С“' => 'a',
					'Р•в„ў' => 'r',
					'Р