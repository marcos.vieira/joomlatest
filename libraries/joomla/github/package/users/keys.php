<?php
/**
 * @package     Joomla.Platform
 * @subpackage  GitHub
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('JPATH_PLATFORM') or die;

/**
 * GitHub API References class for the Joomla Platform.
 *
 * @documentation https://developer.github.com/v3/repos/users/keys
 *
 * @since       12.3
 * @deprecated  4.0  Use the `joomla/github` package via Composer instead
 */
class JGithubPackageUsersKeys extends JGithubPackage
{
	/**
	 * List public keys for a user.
	 *
	 * Lists the verified public keys for a user. This is accessible by anyone.
	 *
	 * @param   string  $user  The name of the user.
	 *
	 * @since 3.3 (CMS)
	 *
	 * @return object
	 */
	public function getListUser($user)
	{
		// Build the request path.
		$path = '/users/' . $user . '/keys';

		return $this->processResponse(
			$this->client->get($this->fetchUrl($path))
		);
	}

	/**
	 * List your public keys.
	 *
	 * Lists the current userРІР‚в„ўs keys.
	 * Management of public keys via the API requires that you are authenticated
	 * through basic auth, or OAuth with the РІР‚