// Hungarian
plupload.addI18n({
    'Select files' : 'FР“РЋjlok kivР“РЋlasztР“РЋsa',
    'Add files to the upload queue and click the start button.' : 'VР“РЋlaszd ki a fР“РЋjlokat, majd kattints az IndР“В­tР“РЋs gombra.',
    'Filename' : 'FР“РЋjlnР“В©v',
    'Status' : 'Р“Рѓllapot',
    'Size' : 'MР“В©ret',
    'Add files' : 'HozzР“РЋadР“РЋs',
    'Stop current upload' : 'Jelenlegi feltР“В¶ltР“В©s megszakР“В­tР“РЋsa',
    'Start uploading queue' : 'VР“РЋrakozР“РЋsi sor feltР“В¶ltР“В©sР“В©nek indР“В­tР“РЋsa',
    'Uploaded %d/%d files': 'FeltР“В¶ltР“В¶tt fР“РЋjlok: %d/%d',
    'N/A': 'Nem elР“В©rhetР•вЂ