// .lv file like language pack
plupload.addI18n({
    'Select files' : 'IzvР”вЂњlieties failus',
    'Add files to the upload queue and click the start button.' : 'Pieveinojiet failus rindai un klikР•РЋР”В·iniet uz "SР”Рѓkt augР•РЋupielР”Рѓdi" pogas.',
    'Filename' : 'Faila nosaukums',
    'Status' : 'Statuss',
    'Size' : 'IzmР”вЂњrs',
    'Add files' : 'Pievienot failus',
    'Stop current upload' : 'ApturР”вЂњt paР•РЋreizР”вЂњjo augР•РЋupielР”Рѓdi',
    'Start uploading queue' : 'SР”Рѓkt augР•РЋupielР”Рѓdi',
    'Drag files here.' : 'Ievelciet failus Р•РЋeit',
    'Start upload' : 'SР”Рѓkt augР•РЋupielР”Рѓdi',
    'Uploaded %d/%d files': 'AugР•РЋupielР”РѓdР”вЂњti %d/%d faili',
    'Stop upload': 'PР”Рѓrtraukt augР•РЋupielР”Рѓdi',
    'Start upload': 'SР”Рѓkt augР•РЋupielР”Рѓdi',
    '%d files queued': '%d faili pievienoti rindai',
    'File: %s': 'Fails: %s',
    'Close': 'AizvР”вЂњrt',
    'Using runtime: ': 'Lieto saskarni: ',
    'File: %f, size: %s, max file size: %m': 'Fails: %f, izmР”вЂњrs: %s, maksimР”Рѓlais faila izmР”вЂњrs: %m',
    'Upload element accepts only %d file(s) at a time. Extra files were stripped.': 'IespР”вЂњjams ielР”РѓdР”вЂњt tikai %d failus vienР”Рѓ reizР”вЂњ. AtlikuР•РЋie faili netika pievienoti',
    'Upload URL might be wrong or doesn\'t exist': 'AugР•РЋupielР”Рѓdes URL varР”вЂњtu bР•В«t nepareizs vai neeksistР”вЂњ',
    'Error: File too large: ': 'KР”С