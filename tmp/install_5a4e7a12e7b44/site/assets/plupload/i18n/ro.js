// Romanian
plupload.addI18n({
    'Select files' : 'Selectare fiР•Сџiere',
    'Add files to the upload queue and click the start button.' : 'AdaugР”С“ fiР•Сџiere Р“В®n lista apoi apasР”С“ butonul \'Р“Р‹ncepe Р“В®ncР”С“rcare\'.',
    'Filename' : 'Nume fiР•Сџier',
    'Status' : 'Stare',
    'Size' : 'MР”С“rime',
    'Add files' : 'AdР”С“ugare fiР•Сџiere',
    'Stop current upload' : 'Р“Р‹ntrerupe Р“В®ncР”С“rcarea curentР”С“',
    'Start uploading queue' : 'Р“Р‹ncepe incР”С“rcarea',
    'Uploaded %d/%d files': 'FiР•Сџiere Р“В®ncР”С“rcate %d/%d',
    'N/A' : 'N/A',
    'Drag files here.' : 'Trage aici fiР•Сџierele',
    'File extension error.': 'Extensie fiР•Сџier eronatР”С“',
    'File size error.': 'Eroare dimensiune fiР•Сџier',
    'Init error.': 'Eroare iniР•Р€ializare',
    'HTTP Error.': 'Eroare HTTP',
    'Security error.': 'Eroare securitate',
    'Generic error.': 'Eroare genericР”С“',
    'IO error.': 'Eroare Intrare/IeР•Сџire',
    'Stop Upload': 'Oprire Р“В®ncР”С“rcare',
    'Start upload': 'Р“Р‹ncepe Р“В®ncР”С“rcare',
    '%d files queued': '%d fiР•Сџiere listate'
});