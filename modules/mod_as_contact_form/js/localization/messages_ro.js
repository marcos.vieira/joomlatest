(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: RO (Romanian, limba romР“СћnР”С“)
 */
$.extend($.validator.messages, {
	required: "Acest cР“Сћmp este obligatoriu.",
	remote: "Te rugР”С“m sР”С“ completezi acest cР“Сћmp.",
	email: "Te rugР”С“m sР”С“ introduci o adresР”С“ de email validР”С“",
	url: "Te rugР”С“m sa introduci o adresР”С“ URL validР”С“.",
	date: "Te rugР”С“m sР”С“ introduci o datР”С“ corectР”С“.",
	dateISO: "Te rugР”С“m sР”С“ introduci o datР”С“ (ISO) corectР”С“.",
	number: "Te rugР”С“m sР”С“ introduci un numР”С“r Р“В®ntreg valid.",
	digits: "Te rugР”С“m sР”С“ introduci doar cifre.",
	creditcard: "Te rugР”С“m sР”С“ introduci un numar de carte de credit valid.",
	equalTo: "Te rugР”С“m sР”С“ reintroduci valoarea.",
	extension: "Te rugР”С“m sР”С“ introduci o valoare cu o extensie validР”С“.",
	maxlength: $.validator.format("Te rugР”С“m sР”С“ nu introduci mai mult de {0} caractere."),
	minlength: $.validator.format("Te rugР”С“m sР”С“ introduci cel puР