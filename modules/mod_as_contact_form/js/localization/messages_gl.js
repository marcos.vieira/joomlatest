(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: GL (Galician; Galego)
 */
(function($) {
	$.extend($.validator.messages, {
		required: "Este campo Р“В© obrigatorio.",
		remote: "Por favor, cubre este campo.",
		email: "Por favor, escribe unha direcciР“С–n de correo vР“РЋlida.",
		url: "Por favor, escribe unha URL vР“РЋlida.",
		date: "Por favor, escribe unha data vР“РЋlida.",
		dateISO: "Por favor, escribe unha data (ISO) vР“РЋlida.",
		number: "Por favor, escribe un nР“С”mero vР“РЋlido.",
		digits: "Por favor, escribe sР“С– dР“В­xitos.",
		creditcard: "Por favor, escribe un nР“С”mero de tarxeta vР“РЋlido.",
		equalTo: "Por favor, escribe o mesmo valor de novo.",
		extension: "Por favor, escribe un valor cunha extensiР“С–n aceptada.",
		maxlength: $.validator.format("Por favor, non escribas mР“РЋis de {0} caracteres."),
		minlength: $.validator.format("Por favor, non escribas menos de {0} caracteres."),
		rangelength: $.validator.format("Por favor, escribe un valor entre {0} e {1} caracteres."),
		range: $.validator.format("Por favor, escribe un valor entre {0} e {1}."),
		max: $.validator.format("Por favor, escribe un valor menor ou igual a {0}."),
		min: $.validator.format("Por favor, escribe un valor maior ou igual a {0}."),
		nifES: "Por favor, escribe un NIF vР“РЋlido.",
		nieES: "Por favor, escribe un NIE vР“РЋlido.",
		cifES: "Por favor, escribe un CIF vР“РЋlido."
	});
}(jQuery));

}));