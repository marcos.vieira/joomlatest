(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: FR (French; franР“В§ais)
 */
$.extend($.validator.messages, {
	required: "Ce champ est obligatoire.",
	remote: "Veuillez corriger ce champ.",
	email: "Veuillez fournir une adresse Р“В©lectronique valide.",
	url: "Veuillez fournir une adresse URL valide.",
	date: "Veuillez fournir une date valide.",
	dateISO: "Veuillez fournir une date valide (ISO).",
	number: "Veuillez fournir un numР“В©ro valide.",
	digits: "Veuillez fournir seulement des chiffres.",
	creditcard: "Veuillez fournir un numР“В©ro de carte de crР“В©dit valide.",
	equalTo: "Veuillez fournir encore la mР“Р„me valeur.",
	extension: "Veuillez fournir une valeur avec une extension valide.",
	maxlength: $.validator.format("Veuillez fournir au plus {0} caractР“РЃres."),
	minlength: $.validator.format("Veuillez fournir au moins {0} caractР“РЃres."),
	rangelength: $.validator.format("Veuillez fournir une valeur qui contient entre {0} et {1} caractР“РЃres."),
	range: $.validator.format("Veuillez fournir une valeur entre {0} et {1}."),
	max: $.validator.format("Veuillez fournir une valeur infР“В©rieure ou Р“В©gale Р“В  {0}."),
	min: $.validator.format("Veuillez fournir une valeur supР“В©rieure ou Р“В©gale Р“В  {0}."),
	maxWords: $.validator.format("Veuillez fournir au plus {0} mots."),
	minWords: $.validator.format("Veuillez fournir au moins {0} mots."),
	rangeWords: $.validator.format("Veuillez fournir entre {0} et {1} mots."),
	letterswithbasicpunc: "Veuillez fournir seulement des lettres et des signes de ponctuation.",
	alphanumeric: "Veuillez fournir seulement des lettres, nombres, espaces et soulignages.",
	lettersonly: "Veuillez fournir seulement des lettres.",
	nowhitespace: "Veuillez ne pas inscrire d'espaces blancs.",
	ziprange: "Veuillez fournir un code postal entre 902xx-xxxx et 905-xx-xxxx.",
	integer: "Veuillez fournir un nombre non dР“В©cimal qui est positif ou nР“В©gatif.",
	vinUS: "Veuillez fournir un numР“В©ro d'identification du vР“В©hicule (VIN).",
	dateITA: "Veuillez fournir une date valide.",
	time: "Veuillez fournir une heure valide entre 00:00 et 23:59.",
	phoneUS: "Veuillez fournir un numР“В©ro de tР“В©lР“В©phone valide.",
	phoneUK: "Veuillez fournir un numР“В©ro de tР“В©lР“В©phone valide.",
	mobileUK: "Veuillez fournir un numР“В©ro de tР“В©lР“В©phone mobile valide.",
	strippedminlength: $.validator.format("Veuillez fournir au moins {0} caractР“РЃres."),
	email2: "Veuillez fournir une adresse Р“В©lectronique valide.",
	url2: "Veuillez fournir une adresse URL valide.",
	creditcardtypes: "Veuillez fournir un numР“В©ro de carte de crР“В©dit valide.",
	ipv4: "Veuillez fournir une adresse IP v4 valide.",
	ipv6: "Veuillez fournir une adresse IP v6 valide.",
	require_from_group: "Veuillez fournir au moins {0} de ces champs.",
	nifES: "Veuillez fournir un numР“В©ro NIF valide.",
	nieES: "Veuillez fournir un numР“В©ro NIE valide.",
	cifES: "Veuillez fournir un numР“В©ro CIF valide.",
	postalCodeCA: "Veuillez fournir un code postal valide."
});

}));