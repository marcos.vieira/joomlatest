(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: CA (Catalan; catalР“В )
 */
$.extend($.validator.messages, {
	required: "Aquest camp Р“В©s obligatori.",
	remote: "Si us plau, omple aquest camp.",
	email: "Si us plau, escriu una adreР“В§a de correu-e vР“В lida",
	url: "Si us plau, escriu una URL vР“В lida.",
	date: "Si us plau, escriu una data vР“В lida.",
	dateISO: "Si us plau, escriu una data (ISO) vР“В lida.",
	number: "Si us plau, escriu un nР“С”mero enter vР“В lid.",
	digits: "Si us plau, escriu nomР“В©s dР“В­gits.",
	creditcard: "Si us plau, escriu un nР“С”mero de tarjeta vР“В lid.",
	equalTo: "Si us plau, escriu el maateix valor de nou.",
	extension: "Si us plau, escriu un valor amb una extensiР“С– acceptada.",
	maxlength: $.validator.format("Si us plau, no escriguis mР“В©s de {0} caracters."),
	minlength: $.validator.format("Si us plau, no escriguis menys de {0} caracters."),
	rangelength: $.validator.format("Si us plau, escriu un valor entre {0} i {1} caracters."),
	range: $.validator.format("Si us plau, escriu un valor entre {0} i {1}."),
	max: $.validator.format("Si us plau, escriu un valor menor o igual a {0}."),
	min: $.validator.format("Si us plau, escriu un valor major o igual a {0}.")
});

}));