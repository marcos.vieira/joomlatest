(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: IS (Icelandic; Р“В­slenska)
 */
$.extend($.validator.messages, {
	required: "Р“С›essi reitur er nauР“В°synlegur.",
	remote: "LagaР“В°u Р“С•ennan reit.",
	maxlength: $.validator.format("SlР“РЋР“В°u inn mest {0} stafi."),
	minlength: $.validator.format("SlР“РЋР“В°u inn minnst {0} stafi."),
	rangelength: $.validator.format("SlР“РЋР“В°u inn minnst {0} og mest {1} stafi."),
	email: "SlР“РЋР“В°u inn gilt netfang.",
	url: "SlР“РЋР“В°u inn gilda vefslР“С–Р“В°.",
	date: "SlР“РЋР“В°u inn gilda dagsetningu.",
	number: "SlР“РЋР“В°u inn tР“В¶lu.",
	digits: "SlР“РЋР“В°u inn tР“В¶lustafi eingР“В¶ngu.",
	equalTo: "SlР“РЋР“В°u sama gildi inn aftur.",
	range: $.validator.format("SlР“РЋР“В°u inn gildi milli {0} og {1}."),
	max: $.validator.format("SlР“РЋР“В°u inn gildi sem er minna en eР“В°a jafnt og {0}."),
	min: $.validator.format("SlР“РЋР“В°u inn gildi sem er stР“В¦rra en eР“В°a jafnt og {0}."),
	creditcard: "SlР“РЋР“В°u inn gilt greiР“В°slukortanР“С”mer."
});

}));