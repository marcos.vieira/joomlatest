(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: CS (Czech; Р”РЊeР•РЋtina, Р”РЊeskР“Р… jazyk)
 */
$.extend($.validator.messages, {
	required: "Tento Р“С”daj je povinnР“Р….",
	remote: "ProsР“В­m, opravte tento Р“С”daj.",
	email: "ProsР“В­m, zadejte platnР“Р… e-mail.",
	url: "ProsР“В­m, zadejte platnР“В© URL.",
	date: "ProsР“В­m, zadejte platnР“В© datum.",
	dateISO: "ProsР“В­m, zadejte platnР“В© datum (ISO).",
	number: "ProsР“В­m, zadejte Р”РЊР“В­slo.",
	digits: "ProsР“В­m, zadР“РЋvejte pouze Р”РЊР“В­slice.",
	creditcard: "ProsР“В­m, zadejte Р”РЊР“В­slo kreditnР“В­ karty.",
	equalTo: "ProsР“В­m, zadejte znovu stejnou hodnotu.",
	extension: "ProsР“В­m, zadejte soubor se sprР“РЋvnou pР•в„ўР“В­ponou.",
	maxlength: $.validator.format("ProsР“В­m, zadejte nejvР“В­ce {0} znakР•Р‡."),
	minlength: $.validator.format("ProsР“В­m, zadejte nejmР“В©nР”вЂє {0} znakР•Р‡."),
	rangelength: $.validator.format("ProsР“В­m, zadejte od {0} do {1} znakР•Р‡."),
	range: $.validator.format("ProsР“В­m, zadejte hodnotu od {0} do {1}."),
	max: $.validator.format("ProsР“В­m, zadejte hodnotu menР•РЋР“В­ nebo rovnu {0}."),
	min: $.validator.format("ProsР“В­m, zadejte hodnotu vР”вЂєtР•РЋР“В­ nebo rovnu {0}.")
});

}));