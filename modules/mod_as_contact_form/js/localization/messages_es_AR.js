(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: ES (Spanish; EspaР“В±ol)
 * Region: AR (Argentina)
 */
$.extend($.validator.messages, {
	required: "Este campo es obligatorio.",
	remote: "Por favor, completР“РЋ este campo.",
	email: "Por favor, escribР“В­ una direcciР“С–n de correo vР“РЋlida.",
	url: "Por favor, escribР“В­ una URL vР“РЋlida.",
	date: "Por favor, escribР“В­ una fecha vР“РЋlida.",
	dateISO: "Por favor, escribР“В­ una fecha (ISO) vР“РЋlida.",
	number: "Por favor, escribР“В­ un nР“С”mero entero vР“РЋlido.",
	digits: "Por favor, escribР“В­ sР“С–lo dР“В­gitos.",
	creditcard: "Por favor, escribР“В­ un nР“С”mero de tarjeta vР“РЋlido.",
	equalTo: "Por favor, escribР“В­ el mismo valor de nuevo.",
	extension: "Por favor, escribР“В­ un valor con una extensiР“С–n aceptada.",
	maxlength: $.validator.format("Por favor, no escribas mР“РЋs de {0} caracteres."),
	minlength: $.validator.format("Por favor, no escribas menos de {0} caracteres."),
	rangelength: $.validator.format("Por favor, escribР“В­ un valor entre {0} y {1} caracteres."),
	range: $.validator.format("Por favor, escribР“В­ un valor entre {0} y {1}."),
	max: $.validator.format("Por favor, escribР“В­ un valor menor o igual a {0}."),
	min: $.validator.format("Por favor, escribР“В­ un valor mayor o igual a {0}."),
	nifES: "Por favor, escribР“В­ un NIF vР“РЋlido.",
	nieES: "Por favor, escribР“В­ un NIE vР“РЋlido.",
	cifES: "Por favor, escribР“В­ un CIF vР“РЋlido."
});

}));