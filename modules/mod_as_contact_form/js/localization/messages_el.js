(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: EL (Greek; РћВµРћВ»РћВ»РћВ·РћР…Рћв„–РћС”РћВ¬)
 */
$.extend($.validator.messages, {
	required: "РћвЂ