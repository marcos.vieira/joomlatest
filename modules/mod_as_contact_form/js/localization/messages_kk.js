(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: KK (Kazakh; РўвЂєР В°Р В·Р В°РўвЂє РЎвЂљРЎвЂ“Р В»РЎвЂ“)
 */
$.extend($.validator.messages, {
	required: "Р вЂ