(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: PL (Polish; jР”в„ўzyk polski, polszczyzna)
 */
$.extend($.validator.messages, {
	required: "To pole jest wymagane.",
	remote: "ProszР”в„ў o wypeР•вЂљnienie tego pola.",
	email: "ProszР”в„ў o podanie prawidР•вЂљowego adresu email.",
	url: "ProszР”в„ў o podanie prawidР•вЂљowego URL.",
	date: "ProszР”в„ў o podanie prawidР•вЂљowej daty.",
	dateISO: "ProszР”в„ў o podanie prawidР•вЂљowej daty (ISO).",
	number: "ProszР”в„ў o podanie prawidР•вЂљowej liczby.",
	digits: "ProszР”в„ў o podanie samych cyfr.",
	creditcard: "ProszР”в„ў o podanie prawidР•вЂљowej karty kredytowej.",
	equalTo: "ProszР”в„ў o podanie tej samej wartoР•вЂєci ponownie.",
	extension: "ProszР”в„ў o podanie wartoР•вЂєci z prawidР•вЂљowym rozszerzeniem.",
	maxlength: $.validator.format("ProszР”в„ў o podanie nie wiР”в„ўcej niР•С