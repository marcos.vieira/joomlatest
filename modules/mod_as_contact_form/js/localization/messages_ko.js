(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: KO (Korean; РЅвЂўСљРєВµВ­РјвЂ“Т‘)
 */
$.extend($.validator.messages, {
	required: "РЅвЂўвЂћРјв‚¬