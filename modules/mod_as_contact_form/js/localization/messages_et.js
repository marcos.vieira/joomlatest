(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: ET (Estonian; eesti, eesti keel)
 */
$.extend($.validator.messages, {
	required: "See vР“В¤li peab olema tР“В¤idetud.",
	maxlength: $.validator.format("Palun sisestage vР“В¤hem kui {0} tР“В¤hemР“В¤rki."),
	minlength: $.validator.format("Palun sisestage vР“В¤hemalt {0} tР“В¤hemР“В¤rki."),
	rangelength: $.validator.format("Palun sisestage vР“В¤Р“В¤rtus vahemikus {0} kuni {1} tР“В¤hemР“В¤rki."),
	email: "Palun sisestage korrektne e-maili aadress.",
	url: "Palun sisestage korrektne URL.",
	date: "Palun sisestage korrektne kuupР“В¤ev.",
	dateISO: "Palun sisestage korrektne kuupР“В¤ev (YYYY-MM-DD).",
	number: "Palun sisestage korrektne number.",
	digits: "Palun sisestage ainult numbreid.",
	equalTo: "Palun sisestage sama vР“В¤Р“В¤rtus uuesti.",
	range: $.validator.format("Palun sisestage vР“В¤Р“В¤rtus vahemikus {0} kuni {1}."),
	max: $.validator.format("Palun sisestage vР“В¤Р“В¤rtus, mis on vР“В¤iksem vР“Вµi vР“Вµrdne arvuga {0}."),
	min: $.validator.format("Palun sisestage vР“В¤Р“В¤rtus, mis on suurem vР“Вµi vР“Вµrdne arvuga {0}."),
	creditcard: "Palun sisestage korrektne krediitkaardi number."
});

}));