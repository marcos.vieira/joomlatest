(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: LT (Lithuanian; lietuviР•С– kalba)
 */
$.extend($.validator.messages, {
	required: "Р•В is laukas yra privalomas.",
	remote: "PraР•РЋau pataisyti Р•РЋР”Р‡ laukР”вЂ¦.",
	email: "PraР•РЋau Р”Р‡vesti teisingР”вЂ¦ elektroninio paР•РЋto adresР”вЂ¦.",
	url: "PraР•РЋau Р”Р‡vesti teisingР”вЂ¦ URL.",
	date: "PraР•РЋau Р”Р‡vesti teisingР”вЂ¦ datР”вЂ¦.",
	dateISO: "PraР•РЋau Р”Р‡vesti teisingР”вЂ¦ datР”вЂ¦ (ISO).",
	number: "PraР•РЋau Р”Р‡vesti teisingР”вЂ¦ skaiР”РЊiР•С–.",
	digits: "PraР•РЋau naudoti tik skaitmenis.",
	creditcard: "PraР•РЋau Р”Р‡vesti teisingР”вЂ¦ kreditinР”вЂ”s kortelР”вЂ”s numerР”Р‡.",
	equalTo: "PraР•РЋau Р”Р‡vestР”Р‡ tР”вЂ¦ paР”РЊiР”вЂ¦ reikР•РЋmР”в„ў dar kartР”вЂ¦.",
	extension: "PraР•РЋau Р”Р‡vesti reikР•РЋmР”в„ў su teisingu plР”вЂ”tiniu.",
	maxlength: $.validator.format("PraР•РЋau Р”Р‡vesti ne daugiau kaip {0} simboliР•С–."),
	minlength: $.validator.format("PraР•РЋau Р”Р‡vesti bent {0} simbolius."),
	rangelength: $.validator.format("PraР•РЋau Р”Р‡vesti reikР•РЋmes, kuriР•С– ilgis nuo {0} iki {1} simboliР•С–."),
	range: $.validator.format("PraР•РЋau Р”Р‡vesti reikР•РЋmР”в„ў intervale nuo {0} iki {1}."),
	max: $.validator.format("PraР•РЋau Р”Р‡vesti reikР•РЋmР”в„ў maР•С•esnР”в„ў arba lygiР”вЂ¦ {0}."),
	min: $.validator.format("PraР•РЋau Р”Р‡vesti reikР•РЋmР”в„ў didesnР”в„ў arba lygiР”вЂ¦ {0}.")
});

}));