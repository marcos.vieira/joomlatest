(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: LV (Latvian; latvieР•РЋu valoda)
 */
$.extend($.validator.messages, {
	required: "Р•В is lauks ir obligР”Рѓts.",
	remote: "LР•В«dzu, pР”Рѓrbaudiet Р•РЋo lauku.",
	email: "LР•В«dzu, ievadiet derР”В«gu e-pasta adresi.",
	url: "LР•В«dzu, ievadiet derР”В«gu URL adresi.",
	date: "LР•В«dzu, ievadiet derР”В«gu datumu.",
	dateISO: "LР•В«dzu, ievadiet derР”В«gu datumu (ISO).",
	number: "LР•В«dzu, ievadiet derР”В«gu numuru.",
	digits: "LР•В«dzu, ievadiet tikai ciparus.",
	creditcard: "LР•В«dzu, ievadiet derР”В«gu kredР”В«tkartes numuru.",
	equalTo: "LР•В«dzu, ievadiet to paР•РЋu vР”вЂњlreiz.",
	extension: "LР•В«dzu, ievadiet vР”вЂњrtР”В«bu ar derР”В«gu paplaР•РЋinР”Рѓjumu.",
	maxlength: $.validator.format("LР•В«dzu, ievadiet ne vairР”Рѓk kР”Рѓ {0} rakstzР”В«mes."),
	minlength: $.validator.format("LР•В«dzu, ievadiet vismaz {0} rakstzР”В«mes."),
	rangelength: $.validator.format("LР•В«dzu ievadiet {0} lР”В«dz {1} rakstzР”В«mes."),
	range: $.validator.format("LР•В«dzu, ievadiet skaitli no {0} lР”В«dz {1}."),
	max: $.validator.format("LР•В«dzu, ievadiet skaitli, kurР•РЋ ir mazР”Рѓks vai vienР”Рѓds ar {0}."),
	min: $.validator.format("LР•В«dzu, ievadiet skaitli, kurР•РЋ ir lielР”Рѓks vai vienР”Рѓds ar {0}.")
});

}));