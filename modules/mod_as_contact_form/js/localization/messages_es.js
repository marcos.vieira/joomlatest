(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: ES (Spanish; EspaР“В±ol)
 */
$.extend($.validator.messages, {
	required: "Este campo es obligatorio.",
	remote: "Por favor, rellena este campo.",
	email: "Por favor, escribe una direcciР“С–n de correo vР“РЋlida.",
	url: "Por favor, escribe una URL vР“РЋlida.",
	date: "Por favor, escribe una fecha vР“РЋlida.",
	dateISO: "Por favor, escribe una fecha (ISO) vР“РЋlida.",
	number: "Por favor, escribe un nР“С”mero vР“РЋlido.",
	digits: "Por favor, escribe sР“С–lo dР“В­gitos.",
	creditcard: "Por favor, escribe un nР“С”mero de tarjeta vР“РЋlido.",
	equalTo: "Por favor, escribe el mismo valor de nuevo.",
	extension: "Por favor, escribe un valor con una extensiР“С–n aceptada.",
	maxlength: $.validator.format("Por favor, no escribas mР“РЋs de {0} caracteres."),
	minlength: $.validator.format("Por favor, no escribas menos de {0} caracteres."),
	rangelength: $.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
	range: $.validator.format("Por favor, escribe un valor entre {0} y {1}."),
	max: $.validator.format("Por favor, escribe un valor menor o igual a {0}."),
	min: $.validator.format("Por favor, escribe un valor mayor o igual a {0}."),
	nifES: "Por favor, escribe un NIF vР“РЋlido.",
	nieES: "Por favor, escribe un NIE vР“РЋlido.",
	cifES: "Por favor, escribe un CIF vР“РЋlido."
});

}));