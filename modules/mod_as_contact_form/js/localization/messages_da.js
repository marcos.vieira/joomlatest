(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: DA (Danish; dansk)
 */
$.extend($.validator.messages, {
	required: "Dette felt er pР“ТђkrР“В¦vet.",
	maxlength: $.validator.format("Indtast hР“С‘jst {0} tegn."),
	minlength: $.validator.format("Indtast mindst {0} tegn."),
	rangelength: $.validator.format("Indtast mindst {0} og hР“С‘jst {1} tegn."),
	email: "Indtast en gyldig email-adresse.",
	url: "Indtast en gyldig URL.",
	date: "Indtast en gyldig dato.",
	number: "Indtast et tal.",
	digits: "Indtast kun cifre.",
	equalTo: "Indtast den samme vР“В¦rdi igen.",
	range: $.validator.format("Angiv en vР“В¦rdi mellem {0} og {1}."),
	max: $.validator.format("Angiv en vР“В¦rdi der hР“С‘jst er {0}."),
	min: $.validator.format("Angiv en vР“В¦rdi der mindst er {0}."),
	creditcard: "Indtast et gyldigt kreditkortnummer."
});

}));