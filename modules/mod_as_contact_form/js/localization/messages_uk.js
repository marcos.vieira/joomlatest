(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: UK (Ukrainian; РЎС“Р С”РЎР‚Р В°РЎвЂ”Р Р…РЎРѓРЎРЉР С”Р В° Р С