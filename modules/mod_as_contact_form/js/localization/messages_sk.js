(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: SK (Slovak; slovenР”РЊina, slovenskР“Р… jazyk)
 */
$.extend($.validator.messages, {
	required: "PovinnР“В© zadaР•Тђ.",
	maxlength: $.validator.format("MaximР“РЋlne {0} znakov."),
	minlength: $.validator.format("MinimР“РЋlne {0} znakov."),
	rangelength: $.validator.format("MinimР“РЋlne {0} a MaximР“РЋlne {1} znakov."),
	email: "E-mailovР“РЋ adresa musР“В­ byР•Тђ platnР“РЋ.",
	url: "URL musР“В­ byР•Тђ platnР“Р….",
	date: "MusР“В­ byР•Тђ dР“РЋtum.",
	number: "MusР“В­ byР•Тђ Р”РЊР“В­slo.",
	digits: "MР“Т‘Р•С•e obsahovaР•Тђ iba Р”РЊР“В­slice.",
	equalTo: "Dva hodnoty sa musia rovnaР•Тђ.",
	range: $.validator.format("MusР“В­ byР•Тђ medzi {0} a {1}."),
	max: $.validator.format("NemР“Т‘Р•С•e byР•Тђ viac ako{0}."),
	min: $.validator.format("NemР“Т‘Р•С•e byР•Тђ menej ako{0}."),
	creditcard: "Р”РЉР“В­slo platobnej karty musР“В­ byР•Тђ platnР“В©."
});

}));