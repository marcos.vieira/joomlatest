<?php
/*******************************************************************************************/
/*
/*        Web: http://www.asdesigning.com
/*        Web: http://www.astemplates.com
/*        License: GNU General Public License
/*
/*******************************************************************************************/

defined('_JEXEC') or die;

require_once __DIR__ . '/helper.php';

$document = JFactory::getDocument();

$document->addStylesheet('modules/mod_as_contact_form/css/style.css');

$labels_pos = $params->get('labels_pos', '');
$success = $params->get('success_notify', '');
$error = $params->get('failure_notify', '');

JHtml::_('jquery.framework', true, null, true);
$document->addScript('modules/mod_as_contact_form/js/jquery.validate.min.js');
$document->addScript('modules/mod_as_contact_form/js/additional-methods.min.js');
$document->addScript('modules/mod_as_contact_form/js/autosize.min.js');
$document->addScriptdeclaration('(function($){$(document).ready(function(){autosize($("textarea"))})})(jQuery);');
$document->addScript('modules/mod_as_contact_form/js/ajaxsendmail.js');

?>

<div class="mod_as_contact_form">

<?php

	require JModuleHelper::getLayoutPath('mod_as_contact_form', $params->get('layout', 'default'));

?>

</div>