/*
LicencovР“РЋno pod MIT LicencР“В­

Р’В© 2008 Seznam.cz, a.s.

TР“В­mto se udР”вЂєluje bezР“С”platnР“РЋ nevР“Р…hradnР“В­ licence kР’В oprР“РЋvnР”вЂєnР“В­ uР•С•Р“В­vat Software,
Р”РЊasovР”вЂє i mР“В­stnР”вЂє neomezenР“РЋ, vР’В souladu sР’В pР•в„ўР“В­sluР•РЋnР“Р…mi ustanovenР“В­mi autorskР“В©ho zР“РЋkona.

Nabyvatel/uР•С•ivatel, kterР“Р… obdrР•С•el kopii tohoto softwaru a dalР•РЋР“В­ pР•в„ўidruР•С•enР“В© 
soubory (dР“РЋle jen РІР‚С›softwareРІР‚Сљ) je oprР“РЋvnР”вЂєn kР’В naklР“РЋdР“РЋnР“В­ se softwarem bez 
jakР“Р…chkoli omezenР“В­, vР”РЊetnР”вЂє bez omezenР“В­ prР“РЋva software uР•С•Р“В­vat, poР•в„ўizovat si 
zР’В nР”вЂєj kopie, mР”вЂєnit, slouР”РЊit, Р•РЋР“В­Р•в„ўit, poskytovat zcela nebo zР”РЊР“РЋsti tР•в„ўetР“В­ osobР”вЂє 
(podlicence) Р”РЊi prodР“РЋvat jeho kopie, za nР“РЋsledujР“В­cР“В­ch podmР“В­nek:

- vР“Р…Р•РЋe uvedenР“В© licenР”РЊnР“В­ ujednР“РЋnР“В­ musР“В­ bР“Р…t uvedeno na vР•РЋech kopiР“В­ch nebo 
podstatnР“Р…ch souР”РЊР“РЋstech Softwaru.

- software je poskytovР“РЋn tak jak stojР“В­ a leР•С•Р“В­, tzn. autor neodpovР“В­dР“РЋ 
za jeho vady, jakoР•С• i moР•С•nР“В© nР“РЋsledky, ledaР•С•e vР”вЂєc nemР“РЋ vlastnost, o nР“В­Р•С• autor 
prohlР“РЋsР“В­, Р•С•e ji mР“РЋ, nebo kterou si nabyvatel/uР•С•ivatel vР“Р…slovnР”вЂє vymР“В­nil.



Licenced under the MIT License

Copyright (c) 2008 Seznam.cz, a.s.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/{if(typeof SZN!="object"){var SZN={"NAME":"SZN"};};SZN.bind=function(obj,fnc){return function(){return fnc.apply(obj,arguments);};};SZN.idGenerator=function(){this.idCnt=this.idCnt<10000000?this.idCnt:0;var ids="m"+new Date().getTime().toString(16)+"m"+this.idCnt.toString(16);this.idCnt++;return ids;};if(!Array.prototype.indexOf){Array.prototype.indexOf=function(item,from){var len=this.length;var i=from||0;if(i<0){i+=len;}for(;i<len;i++){if(i in this&&this[i]===item){return i;}}return -1;};}if(!Array.indexOf){Array.indexOf=function(obj,item,from){return Array.prototype.indexOf.call(obj,item,from);};}if(!Array.prototype.lastIndexOf){Array.prototype.lastIndexOf=function(item,from){var len=this.length;var i=from||len-1;if(i<0){i+=len;}for(;i>-1;i--){if(i in this&&this[i]===item){return i;}}return -1;};}if(!Array.lastIndexOf){Array.lastIndexOf=function(obj,item,from){return Array.prototype.lastIndexOf.call(obj,item,from);};}if(!Array.prototype.forEach){Array.prototype.forEach=function(cb,_this){var len=this.length;for(var i=0;i<len;i++){if(i in this){cb.call(_this,this[i],i,this);}}};}if(!Array.forEach){Array.forEach=function(obj,cb,_this){Array.prototype.forEach.call(obj,cb,_this);};}if(!Array.prototype.every){Array.prototype.every=function(cb,_this){var len=this.length;for(var i=0;i<len;i++){if(i in this&&!cb.call(_this,this[i],i,this)){return false;}}return true;};}if(!Array.every){Array.every=function(obj,cb,_this){return Array.prototype.every.call(obj,cb,_this);};}if(!Array.prototype.some){Array.prototype.some=function(cb,_this){var len=this.length;for(var i=0;i<len;i++){if(i in this&&cb.call(_this,this[i],i,this)){return true;}}return false;};}if(!Array.some){Array.some=function(obj,cb,_this){return Array.prototype.some.call(obj,cb,_this);};}if(!Array.prototype.map){Array.prototype.map=function(cb,_this){var len=this.length;var res=new Array(len);for(var i=0;i<len;i++){if(i in this){res[i]=cb.call(_this,this[i],i,this);}}return res;};}if(!Array.map){Array.map=function(obj,cb,_this){return Array.prototype.map.call(obj,cb,_this);};}if(!Array.prototype.filter){Array.prototype.filter=function(cb,_this){var len=this.length;var res=[];for(var i=0;i<len;i++){if(i in this){var val=this[i];if(cb.call(_this,val,i,this)){res.push(val);}}}return res;};}if(!Array.filter){Array.filter=function(obj,cb,_this){return Array.prototype.filter.call(obj,cb,_this);};}String.prototype.lpad=function(char,count){var ch=char||"0";var cnt=count||2;var s="";while(s.length<(cnt-this.length)){s+=ch;}s=s.substring(0,cnt-this.length);return s+this;};String.prototype.rpad=function(char,count){var ch=char||"0";var cnt=count||2;var s="";while(s.length<(cnt-this.length)){s+=ch;}s=s.substring(0,cnt-this.length);return this+s;};String.prototype.trim=function(){return this.match(/^\s*([\s\S]*?)\s*$/)[1];};Date.prototype._dayNames=["PondР”вЂєlР“В­","Р“С™terР“Р…","StР•в„ўeda","Р”РЉtvrtek","PР“РЋtek","Sobota","NedР”вЂєle"];Date.prototype._dayNamesShort=["Po","Р“С™t","St","Р”РЉt","PР“РЋ","So","Ne"];Date.prototype._monthNames=["Leden","Р“С™nor","BР•в„ўezen","Duben","KvР”вЂєten","Р”РЉerven","Р”РЉervenec","Srpen","ZР“РЋР•в„ўР“В­","Р•