/**@license boxplus strings for localization
 * @author  Levente Hunyadi
 * @version 0.9.3
 * @remarks Copyright (C) 2010-2011 Levente Hunyadi
 * @remarks Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
 * @see     http://hunyadi.info.hu/projects/boxplus
 **/

/*
* Requires MooTools 1.2 or later.
* Annotated for use with Google Closure Compiler's advanced optimization
* method when supplemented with a MooTools extern file.
*/

;
(function () {
	/** Language strings. */
	var localizations = {
		'en': {
			language: 'English',
			first: 'First',
			prev: 'Previous',
			next: 'Next',
			last: 'Last',
			start: 'Start slideshow',
			stop: 'Stop slideshow',
			close: 'Close',
			enlarge: 'Enlarge',
			shrink: 'Shrink',
			download: 'Download',
			metadata: 'Image metadata',
			notFound: 'Document not found or not available for download.',
			unknownType: 'The embedded content to be displayed in this window is of type <code>%s</code>, which is not currently supported by your browser.\nInstalling the appropriate browser plug-in usually solves the problem.'
		},
		'de': {
			language: 'Deutsch',
			first: 'Erstes',
			prev: 'ZurР“С