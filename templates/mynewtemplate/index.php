<?php 
	defined( '_JEXEC' ) or die( 'Restricted access' );

JLoader::import('joomla.filesystem.file');

JHtml::_('behavior.framework', true);

// Get params
// $color          = $this->params->get('templatecolor');
// $navposition    = $this->params->get('navposition');
// $headerImage    = $this->params->get('headerImage');
// $bootstrap      = explode(',', $this->params->get('bootstrap'));
$logo           = $this->params->get('logo');
$config         = JFactory::getConfig();
$option         = JFactory::getApplication()->input->getCmd('option', '');

// Output as HTML5
$this->setHtml5(true);

// Add template scripts

// Add template css
JHtml::_('stylesheet', 'templates/system/css/system.css', array('version' => 'auto'));
JHtml::_('stylesheet', 'templates/system/css/general.css', array('version' => 'auto'));
JHtml::_('stylesheet', 'templates/'.$this->template.'/css/template.css', array('version' => 'auto'));

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" 
   xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >

<head>
	<jdoc:include type="head"/>
	<link rel="stylesheet" type="text/css" href="templates/bootstrap4/css/bootstrap.min.css">
	<script type="text/javascript" src="templates/bootstrap4/css/bootstrap.min.css"></script>
</head>

<body class="col-xs-12">
	<jdoc:include type="modules" name="header" />

	<?php echo $this->baseurl ?> 
	<jdoc:include type="component" />
	<!-- <jdoc:include type="modules" name="top" /> 
	<jdoc:include type="modules" name="bottom" /> -->


	<?php echo htmlspecialchars($this->params->get('sitedescription')); ?>
	<header class="header-principal">
		<?php if ($logo): ?>
			<img src="<?php echo $this->baseurl; ?>/<?php echo htmlspecialchars($logo); ?>"  alt="<?php echo htmlspecialchars($this->params->get('sitetitle')); ?>" />
		<?php endif ?>
	</header>

	<ul class="menu-lateral col-sm-3">
		<jdoc:include type="modules" name="left">
	</ul>
</body>

</html>