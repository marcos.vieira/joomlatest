<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.ostrainingbreeze
 *
 * @copyright   Copyright (C) 2009, 2013 OSTraining.com
 * @license     GNU General Public License version 2 or later; see license.txt
 */

defined('_JEXEC') or die;

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

// Adjusting main content width
if ($this->countModules('left') && $this->countModules('right')) {
    $span_component = "span6";
} elseif ($this->countModules('left') && !$this->countModules('right')) {
    $span_component = "span9";
} elseif (!$this->countModules('left') && $this->countModules('right')) {
    $span_component = "span9";
} else {
    $span_component = "span12";
}

// Adjusting bottom width
if ($this->countModules('bottom-a and bottom-b and bottom-c')) {
    $span_bottom = "span4";
} elseif ($this->countModules('bottom-a xor bottom-b xor bottom-c')) {
    $span_bottom = "span12";
} else {
    $span_bottom = "span6";
}

// Adjusting footer width
if ($this->countModules('footer-a and footer-b and footer-c')) {
    $span_footer = "span4";
} elseif ($this->countModules('footer-a xor footer-b xor footer-c')) {
    $span_footer = "span12";
} else {
    $span_footer = "span6";
}

// Logo file
if ($this->params->get('logoFile')) {
    $logo = '<img src="' . $this->baseurl . '/' . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
} else {
    $logo = '<img src="' . $this->baseurl . '/templates/' . $this->template . '/images/logo.png" alt="' . $sitename . '" />';
}

// color scheme
$color_scheme = $this->params->get('colorScheme', '#2184CD');

// hover color
$hover_color = $this->params->get('hoverColor', '#41A1D6');

// Google font
$google_font = explode(':', $this->params->get('googleFontName'));

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript('templates/bootstrap4/js/bootstrap.min.js');
$doc->addScript('templates/' . $this->template . '/js/template.js');
$doc->addScript('templates/' . $this->template . '/js/jquery.mobilemenu.js');

// Add Stylesheets
$doc->addStyleSheet('templates/bootstrap4/css/bootstrap.min.css');
$doc->addStyleSheet('templates/system/css/general.css');
$doc->addStyleSheet('templates/' . $this->template . '/css/template.css');

// Mobile menu
if ($this->params->get('mobileMenu', 1)) {
    $doc->addScript('templates/' . $this->template . '/js/mobilemenu.js');
    $doc->addStyleSheet('templates/' . $this->template . '/css/mobilemenu.css');
}

// font awesome
if ($this->params->get('fontAwesome', 1)) {
    $doc->addStyleSheet('templates/' . $this->template . '/css/font-awesome/css/font-awesome.min.css');
}

// custom css
if (file_exists('templates/' . $this->template . '/css/custom.css')) {
    $doc->addStyleSheet('templates/' . $this->template . '/css/custom.css');
}

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<jdoc:include type="head" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <style>
        a{
            color:<?php echo $color_scheme; ?>;
        }
        #footer{
            border-color:<?php echo $color_scheme; ?>;
        }
        .btn-primary,
        .navigation,
        #mainmenu > li > ul > li > a:hover,
        .navigation .nav-child li > a:hover,
        .navigation .nav-child li > a:focus,
        .navigation .nav-child:hover > a,
        .nav > li a,
        #mainmenu > li > ul > li > span:hover,
        .navigation .nav-child li > span:hover,
        .navigation .nav-child li > span:focus,
        .navigation .nav-child:hover > span,
        .nav > li span {
            background-color:<?php echo $color_scheme; ?>;
        }
        #mainmenu > .active > a,
        #mainmenu > .active > a:hover,
        #mainmenu > .active > a:focus,
        #mainmenu > .active > span,
        #mainmenu > .active > span:hover,
        #mainmenu > .active > span:focus,
        #mainmenu > li > a:hover,
        #mainmenu > li > a:focus,
        #mainmenu > li > span:hover,
        #mainmenu > li > span:focus {
            background-color: <?php echo $hover_color; ?>;
        }
    </style>
    <?php
// Use of Google Font
if ($this->params->get('googleFont')) {
    ?>
        <link href='//fonts.googleapis.com/css?family=<?php echo $this->params->get('googleFontName'); ?>' rel='stylesheet' type='text/css' />
        <style type="text/css">
            body{
                font-family: '<?php echo str_replace('+', ' ', $google_font[0]); ?>', sans-serif;
            }
        </style>
    <?php
}
?>
    <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body class="myitemid-<?php echo $itemid; ?>">

    <!-- Body -->
    <div class="body">
        <div class="container">
            <!-- Header -->
                <div class="row-fluid" id="top-brand">
                    <div class="span3">
                        <a class="brand pull-left" href="<?php echo $this->baseurl; ?>">
                            <?php echo $logo; ?>
                        </a>
                    </div>
                    <div class="span9">
                        <div class="header-search pull-right">
                            <jdoc:include type="modules" name="top" style="none" />
                        </div>
                    </div>
                </div>
            <?php if ($this->countModules('menu')): ?>
                <nav class="navigation" role="navigation">
                    <jdoc:include type="modules" name="menu" style="none" />
                </nav>
            <?php endif;?>
            <jdoc:include type="modules" name="banner" style="well" />
            <div class="row-fluid">
                <?php if ($this->countModules('left')): ?>
                    <!-- Start Left -->
                    <div id="left-content" class="span3">
                        <div class="sidebar-nav">
                            <jdoc:include type="modules" name="left" style="xhtml" />
                        </div>
                    </div>
                    <!-- Start Left -->
                <?php endif;?>
                <!-- Start Content -->
                <div id="content" role="main" class="<?php echo $span_component; ?>">
                    <jdoc:include type="modules" name="bodytop" style="xhtml" />
                    <jdoc:include type="message" />
                    <jdoc:include type="component" />
                    <jdoc:include type="modules" name="bodybottom" style="xhtml" />
                </div>
                <!-- End Content -->
                <?php if ($this->countModules('right')): ?>
                    <div id="right-content" class="span3">
                        <!-- Start Right -->
                        <jdoc:include type="modules" name="right" style="xhtml" />
                        <!-- End Right -->
                    </div>
                <?php endif;?>
            </div>
            <!-- Start bottom -->
            <?php if ($this->countModules('bottom-a or bottom-b or bottom-c')): ?>
                <div class="row-fluid" id="bottom">
                    <?php if ($this->countModules('bottom-a')): ?>
                        <!-- Start bottom-a -->
                        <div id="bottom-a" class="<?php echo $span_bottom; ?>">
                            <jdoc:include type="modules" name="bottom-a" style="well" />
                        </div>
                        <!-- End bottom-a -->
                    <?php endif;?>
                    <?php if ($this->countModules('bottom-b')): ?>
                        <!-- Start bottom-b -->
                        <div id="bottom-b" class="<?php echo $span_bottom; ?>">
                            <jdoc:include type="modules" name="bottom-b" style="well" />
                        </div>
                        <!-- End bottom-b -->
                    <?php endif;?>
                    <?php if ($this->countModules('bottom-c')): ?>
                        <!-- Start bottom-c -->
                        <div id="bottom-c" class="<?php echo $span_bottom; ?>">
                            <jdoc:include type="modules" name="bottom-c" style="well" />
                        </div>
                        <!-- End bottom-c -->
                    <?php endif;?>
                </div>
            <?php endif;?>
            <!-- End bottom -->
        </div>
    </div>


    <!-- ESTRUTURA DO FOOTER ANTIGO -->
     <!-- <footer class="footer" role="contentinfo">
        <div class="container">
            <jdoc:include type="modules" name="footer" style="xhtml" />

            <?php if ($this->countModules('footer-a or footer-b or footer-c')): ?>
                <div class="row-fluid" id="footer">
                    <?php if ($this->countModules('footer-a')): ?>

                        <div id="footer-a" class="<?php echo $span_footer; ?>">
                            <jdoc:include type="modules" name="footer-a" style="footer" />
                        </div>

                    <?php endif;?>
                    <?php if ($this->countModules('footer-b')): ?>

                        <div id="footer-b" class="<?php echo $span_footer; ?>">
                            <jdoc:include type="modules" name="footer-b" style="footer" />
                        </div>

                    <?php endif;?>
                    <?php if ($this->countModules('footer-c')): ?>

                        <div id="footer-c" class="<?php echo $span_footer; ?>">
                            <jdoc:include type="modules" name="footer-c" style="footer" />
                        </div>
                    <?php endif;?>
                </div>
            <?php endif;?>
            <div class="row-fluid copyright-ost">
                <div class="span6">&copy; <?php echo $sitename; ?> <?php echo date('Y'); ?></div>
                <div class="span6"><jdoc:include type="modules" name="credits" style="no" /></div>
            </div>
        </div>
    </footer> -->

<footer class="new-footer">
    <div class="container">
    <jdoc:include type="modules" name="footer"/>
        <div class="row-fluid">
                <div class="col-sm-3 col-xs-12 float-left" style="">
                    <ul class="unstyled">
                        <li class="title">Institucional<li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=1">Quem Somos</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=2">Conselho</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=3">Diretorias e Coordenadorias</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=4">Nossa HistР“С–ria</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=6">MissР“Р€o/PrincР“В­pio</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=8">Departamentos</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=7">Ouvidoria</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 col-xs-12 float-left" style="">
                    <ul class="unstyled">
                        <li class="title">Social<li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=9">Sobre</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=10">Grupo da Amizade</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=12">Fale com o Social</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 col-xs-12 float-left" style="">
                    <ul class="unstyled">
                        <li class="title">Р“Рѓrea do Associado<li>
                        <li><a href="#">Como acessar?</a></li>
                        <li><a href="#">IdentificaР“В§Р“Р€o</a></li>
                        <li><a href="#">Alterar Meus Dados</a></li>
                        <li><a href="#">Carteira Social/Dependentes</a></li>
                        <li><a href="#">Definir Nova Senha</a></li>
                        <li><a href="#">Pagamento - DР“В©bito</a></li>
                        <li><a href="#">Pagamento - Boletos</a></li>
                        <li><a href="#">Pagamento - 2Р’Р„ Via ItaР“С”</a></li>
                        <li><a href="#">Pesquisa de SatisfaР“В§Р“Р€o</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 col-xs-12 float-left" style="">
                    <ul class="unstyled">
                        <li class="title">Turismo<li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=15">Sobre</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=16">ExcursР“Вµes</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=17">Unidades Regionais</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=18">Parques</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=19">HotР“В©is Conveniados</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=20">Aconteceu</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=21">DocumentaР“В§Р“Р€o</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=category&id=11">Dicas do Viajante</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=22">Fale com o Turismo</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 col-xs-12 float-left" style="">
                    <ul class="unstyled">
                        <li class="title">Reservas<li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=15">Sobre</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=16">FlexReserva</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=17">Pagamentos</a></li>
                        <li><a href="<?php $this->baseurl?>index.php?option=com_content&view=article&id=18">Fale com Reservas</a></li>
                    </ul>
                </div>
            </div>
            <hr>
            <div class="row-fluid copyright-ost">
                <div class="span6">&copy; <?php echo $sitename; ?> <?php echo date('Y'); ?></div>
                <div class="span6"><jdoc:include type="modules" name="credits" style="no" /></div>
            </div>
        </div>
    </div>
</footer>

    <jdoc:include type="modules" name="debug" style="none" />
</body>
</html>