// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: http://codemirror.net/LICENSE

(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})(function(CodeMirror) {
"use strict";

CodeMirror.defineMode("apl", function() {
  var builtInOps = {
    ".": "innerProduct",
    "\\": "scan",
    "/": "reduce",
    "РІРЉС—": "reduce1Axis",
    "РІРЊР‚": "scan1Axis",
    "Р’РЃ": "each",
    "РІРЊР€": "power"
  };
  var builtInFuncs = {
    "+": ["conjugate", "add"],
    "РІв‚¬вЂ™": ["negate", "subtract"],
    "Р“вЂ”": ["signOf", "multiply"],
    "Р“В·": ["reciprocal", "divide"],
    "РІРЉв‚¬": ["ceiling", "greaterOf"],
    "РІРЉР‰": ["floor", "lesserOf"],
    "РІв‚¬Р€": ["absolute", "residue"],
    "РІРЊС–": ["indexGenerate", "indexOf"],
    "?": ["roll", "deal"],
    "РІвЂ№вЂ ": ["exponentiate", "toThePowerOf"],
    "РІРЊСџ": ["naturalLog", "logToTheBase"],
    "РІвЂ”вЂ№": ["piTimes", "circularFuncs"],
    "!": ["factorial", "binomial"],
    "РІРЉв„–": ["matrixInverse", "matrixDivide"],
    "<": [null, "lessThan"],
    "РІвЂ°В¤": [null, "lessThanOrEqual"],
    "=": [null, "equals"],
    ">": [null, "greaterThan"],
    "РІвЂ°Тђ": [null, "greaterThanOrEqual"],
    "РІвЂ°В ": [null, "notEqual"],
    "РІвЂ°РЋ": ["depth", "match"],
    "РІвЂ°Сћ": [null, "notMatch"],
    "РІв‚¬в‚¬": ["enlist", "membership"],
    "РІРЊВ·": [null, "find"],
    "РІв‚¬Р„": ["unique", "union"],
    "РІв‚¬В©": [null, "intersection"],
    "РІв‚¬С