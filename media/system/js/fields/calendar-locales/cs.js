window.JoomlaCalLocale = {
	today : "dnes",
	weekend : [0, 6],
	wk : "kt",
	time : "Р”РЊas:",
	days : ["nedР”вЂєle", "pondР”вЂєlР“В­", "Р“С”terР“Р…", "stР•в„ўeda", "Р”РЊtvrtek", "pР“РЋtek", "sobota"],
	shortDays : ["ne", "po", "Р“С”t", "st", "Р”РЊt", "pР“РЋ", "so"],
	months : ["leden", "Р“С”nor", "bР•в„ўezen", "duben", "kvР”вЂєten", "Р”РЊerven", "Р”РЊervenec", "srpen", "zР“РЋР•в„ўР“В­", "Р•в„ўР“В­jen", "listopad", "prosinec"],
	shortMonths : ["led", "Р“С”no", "bР•в„ўe", "dub", "kvР”вЂє", "Р”РЊen", "Р”РЊec", "srp", "zР“РЋР•в„ў", "Р•в„ўР“В­j", "lis", "pro"],
	AM : "AM",
	PM :  "PM",
	am : "am",
	pm : "pm",
	dateType : "gregorian",
	minYear : 1900,
	maxYear : 2100,
	exit: "zavР•в„ўР“В­t",
	save: "vymazat"
};