window.JoomlaCalLocale = {
	today : "Dzisiaj",
	weekend : [0, 6],
	wk : "tydz",
	time : "Godzina:",
	days : ["Niedziela", "PoniedziaР•вЂљek", "Wtorek", "Р•С™roda", "Czwartek", "PiР”вЂ¦tek", "Sobota"],
	shortDays : ["nie.", "pon.", "wt.", "Р•вЂєr.", "czw.", "pt.", "sob."],
	months : ["StyczeР•вЂћ", "Luty", "Marzec", "KwiecieР•вЂћ", "Maj", "Czerwiec", "Lipiec", "SierpieР•вЂћ", "WrzesieР•вЂћ", "PaР•С”dziernik", "Listopad", "GrudzieР•вЂћ"],
	shortMonths : ["Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie", "Wrz", "PaР•С”", "Lis", "Gru"],
	AM : "AM",
	PM :  "PM",
	am : "am",
	pm : "pm",
	dateType : "gregorian",
	minYear : 1900,
	maxYear : 2100,
	exit: "Zamknij",
	save: "WyczyР•вЂєР”вЂЎ"
};