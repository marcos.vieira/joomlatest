window.JoomlaCalLocale = {
	today : "Dagens dato",
	weekend : [0, 6],
	wk : "Uke",
	time : "Tid:",
	days : ["SР“С‘ndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "LР“С‘rdag"],
	shortDays : ["SР“С‘n", "Man", "Tir", "Ons", "Tor", "Fre", "LР“С‘r"],
	months : ["Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Desember"],
	shortMonths : ["Jan", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Des"],
	AM : "AM",
	PM :  "PM",
	am : "am",
	pm : "pm",
	dateType : "gregorian",
	minYear : 1900,
	maxYear : 2100,
	exit: "Lukk",
	save: "TР“С‘m"
};
