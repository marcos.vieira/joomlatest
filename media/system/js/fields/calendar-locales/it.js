window.JoomlaCalLocale = {
	today : "Oggi",
	weekend : [0, 6],
	wk : "set",
	time : "Ora:",
	days : ["Domenica", "LunedР“В¬", "MartedР“В¬", "MercoledР“В¬", "GiovedР“В¬", "VenerdР“В¬", "Sabato"],
	shortDays : ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"],
	months : ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],
	shortMonths : ["Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"],
	AM : "AM",
	PM :  "PM",
	am : "am",
	pm : "pm",
	dateType : "gregorian",
	minYear : 1900,
	maxYear : 2100,
	exit: "Chiudi",
	save: "Annulla"
};