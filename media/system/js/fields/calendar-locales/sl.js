window.JoomlaCalLocale = {
	today : "Danes",
	weekend : [0, 6],
	wk : "wk",
	time : "Р”РЉas:",
	days : ["Nedelja", "Ponedeljek", "Torek", "Sreda", "Р”РЉetrtek", "Petek", "Sobota"],
	shortDays : ["Ned", "Pon", "Tor", "Sre", "Р”РЉet", "Pet", "Sob"],
	months : ["Januar", "Februar", "Marec", "April", "Maj", "Junij", "Julij", "Avgust", "September", "Oktober", "November", "December"],
	shortMonths : ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Avg", "Sep", "Okt", "Nov", "Dec"],
	AM : "AM",
	PM :  "PM",
	am : "am",
	pm : "pm",
	dateType : "gregorian",
	minYear : 1900,
	maxYear : 2100,
	exit: "Zapri",
	save: "PoР”РЊisti"
};