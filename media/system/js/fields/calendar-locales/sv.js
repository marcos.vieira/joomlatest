window.JoomlaCalLocale = {
	today : "Idag",
	weekend : [0, 6],
	wk : "vk",
	time : "Tid:",
	days : ["SР“В¶ndag", "MР“Тђndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", "LР“В¶rdag"],
	shortDays : ["SР“В¶n", "MР“Тђn", "Tis", "Ons", "Tor", "Fre", "LР“В¶r"],
	months : ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"],
	shortMonths : ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"],
	AM : "FM",
	PM :  "EM",
	am : "fm",
	pm : "em",
	dateType : "gregorian",
	minYear : 1900,
	maxYear : 2100,
	exit: "StР“В¤ng",
	save: "Rensa"
};