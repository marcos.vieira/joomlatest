window.JoomlaCalLocale = {
	today : "Inniu",
	weekend : [0, 6],
	wk : "7n",
	time : "Am:",
	days : ["DР“В© Domhnaigh", "DР“В© Luain", "DР“В© MР“РЋirt", "DР“В© CР“В©adaoin", "DР“В©ardaoin", "DР“В© hAoine", "DР“В© Sathairn"],
	shortDays : ["Domh", "Luan", "MР“РЋirt", "CР“В©ad", "DР“В©ar", "Aoine", "Sath"],
	months : ["EanР“РЋir", "Feabhra", "MР“РЋrta", "AibreР“РЋn", "Bealtaine", "Meitheamh", "IР“С”il", "LР“С”nasa", "MeР“РЋn FР“С–mhair", "Deireadh FР“С–mhair", "Samhain", "Nollaig"],
	shortMonths : ["Ean", "Feabh", "MР“РЋrta", "Aib", "Beal", "Meith", "IР“С”il", "LР“С”n", "MFР“С–mh", "DFР“С–mh", "Samh", "Noll"],
	AM : "AM",
	PM :  "PM",
	am : "am",
	pm : "pm",
	dateType : "gregorian",
	minYear : 1900,
	maxYear : 2100,
	exit: "DР“С”n",
	save: "Glan"
};
