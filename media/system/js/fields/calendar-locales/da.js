window.JoomlaCalLocale = {
	today : "I dag",
	weekend : [0, 6],
	wk : "uge",
	time : "Tid:",
	days : ["SР“С‘ndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "LР“С‘rdag"],
	shortDays : ["SР“С‘n", "Man", "Tir", "Ons", "Tor", "Fre", "LР“С‘r"],
	months : ["Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December"],
	shortMonths : ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"],
	AM : "AM",
	PM :  "PM",
	am : "am",
	pm : "pm",
	dateType : "gregorian",
	minYear : 1900,
	maxYear : 2100,
	exit: "Luk",
	save: "Nulstil"
};