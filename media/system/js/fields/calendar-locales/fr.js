window.JoomlaCalLocale = {
	today : "Aujourd'hui",
	weekend : [0, 6],
	wk : "wk",
	time : "Heure&nbsp;:",
	days : ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
	shortDays : ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
	months : ["Janvier", "FР“В©vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "AoР“В»t", "Septembre", "Octobre", "Novembre", "DР“В©cembre"],
	shortMonths : ["Jan", "FР“В©v", "Mar", "Avr", "Mai", "Jui", "Juol", "AoР“В»", "Sep", "Oct", "Nov", "DР“В©c"],
	AM : "AM",
	PM :  "PM",
	am : "am",
	pm : "pm",
	dateType : "gregorian",
	minYear : 1900,
	maxYear : 2100,
	exit: "Fermer",
	save: "Effacer"
};