window.JoomlaCalLocale = {
	today : "Danas",
	weekend : [0, 6],
	wk : "se",
	time : "Vrijeme:",
	days : ["Nedjelja", "Ponedjeljak", "Utorak", "Srijeda", "Р”РЉetvrtak", "Petak", "Subota"],
	shortDays : ["Ned", "Pon", "Uto", "Sri", "Р”РЉet", "Pet", "Sub"],
	months : ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"],
	shortMonths : ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Avg", "Sep", "Okt", "Nov", "Dec"],
	AM : "AM",
	PM :  "PM",
	am : "am",
	pm : "pm",
	dateType : "gregorian",
	minYear : 1900,
	maxYear : 2100,
	exit: "Zatvori",
	save: "Snimi"
};