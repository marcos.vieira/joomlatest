window.JoomlaCalLocale = {
	today : "TР“В¤nР“В¤Р“В¤n",
	weekend : [0, 6],
	wk : "vk",
	time : "Aika:",
	days : ["Sunnuntai", "Maanantai", "Tiistai", "Keskiviikko", "Torstai", "Perjantai", "Lauantai"],
	shortDays : ["Su", "Ma", "Ti", "Ke", "To", "Pe", "La"],
	months : ["Tammikuu", "Helmikuu", "Maaliskuu", "Huhtikuu", "Toukokuu", "KesР“В¤kuu", "HeinР“В¤kuu", "Elokuu", "Syyskuu", "Lokakuu", "Marraskuu", "Joulukuu"],
	shortMonths : ["Tammi", "Helmi", "Maalis", "Huhti", "Touko", "KesР“В¤", "HeinР“В¤", "Elo", "Syys", "Loka", "Marras", "Joulu"],
	AM : "AP",
	PM :  "IP",
	am : "ap",
	pm : "ip",
	dateType : "gregorian",
	minYear : 1900,
	maxYear : 2100,
	exit: "Sulje",
	save: "TyhjennР“В¤"
};