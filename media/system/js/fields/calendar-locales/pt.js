window.JoomlaCalLocale = {
	today : "Hoje",
	weekend : [0, 6],
	wk : "sem",
	time : "Hora:",
	days : ["Domingo", "Segunda-feira", "TerР“В§a-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "SР“РЋbado"],
	shortDays : ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
	months : ["Janeiro", "Fevereiro", "MarР“В§o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
	shortMonths : ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
	AM : "AM",
	PM :  "PM",
	am : "am",
	pm : "pm",
	dateType : "gregorian",
	minYear : 1900,
	maxYear : 2100,
	exit: "Fechar",
	save: "Limpar"
};
