window.JoomlaCalLocale = {
	today : "Danas",
	weekend : [0, 6],
	wk : "tj",
	time : "Vrijeme:",
	days : ["Nedjelja", "Ponedjeljak", "Utorak", "Srijeda", "Р”РЉetvrtak", "Petak", "Subota"],
	shortDays : ["Ned", "Pon", "Uto", "Sri", "Р”РЉet", "Pet", "Sub"],
	months : ["SijeР”РЊanj", "VeljaР”РЊa", "OР•С•ujak", "Travanj", "Svibanj", "Lipanj", "Srpanj", "Kolovoz", "Rujan", "Listopad", "Studeni", "Prosinac"],
	shortMonths : ["Sij", "Velj", "OР•С•u", "Tra", "Svi", "Lip", "Srp", "Kol", "Ruj", "Lis", "Stu", "Pro"],
	AM : "AM",
	PM :  "PM",
	am : "am",
	pm : "pm",
	dateType : "gregorian",
	minYear : 1900,
	maxYear : 2100,
	exit: "Zatvori",
	save: "OtkaР•С•i"
};